package config

import (
	migration "assignment_2/migrations"
	"fmt"
	"os"
	"strconv"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

var Db *gorm.DB

// DBConfig represents db configuration
type DBConfig struct {
	Host     string
	Port     int
	User     string
	DBName   string
	Password string
}

func InitDB() error {
	var err error
	Db, err = gorm.Open(mysql.Open(DbURL(BuildDBConfig())), &gorm.Config{})
	if err != nil {
		return err
	}
	//Turn these off if Alrd Migrated
	Db.Migrator().DropTable(migration.Item{})
	Db.Migrator().DropTable(migration.Order{})
	Migrate(Db)
	//End Turn

	return nil
}

func GetDB() *gorm.DB {
	return Db
}

func BuildDBConfig() *DBConfig {
	port, _ := strconv.Atoi(os.Getenv("APP_DB_PORT"))
	dbConfig := DBConfig{
		Host:     os.Getenv("APP_DB_HOST"),
		Port:     port,
		User:     os.Getenv("APP_DB_USERNAME"),
		Password: os.Getenv("APP_DB_PASSWORD"),
		DBName:   os.Getenv("APP_DB_NAME"),
	}
	return &dbConfig
}
func DbURL(dbConfig *DBConfig) string {
	return fmt.Sprintf(
		"%s:%s@tcp(%s:%d)/%s?charset=utf8&parseTime=True&loc=Local",
		dbConfig.User,
		dbConfig.Password,
		dbConfig.Host,
		dbConfig.Port,
		dbConfig.DBName,
	)
}

func Migrate(db *gorm.DB) {
	var order migration.Order
	var item migration.Item
	OrderProto := &order
	ItemProto := &item

	db.AutoMigrate(ItemProto, OrderProto)
	db.Migrator().CreateConstraint(ItemProto, "Orders")
	db.Migrator().CreateConstraint(ItemProto, "fk_items_orders")
}
