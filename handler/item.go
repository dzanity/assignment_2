package handler

import (
	"assignment_2/models"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
)

func CreateItem(c *gin.Context) {
	var item models.Item
	c.BindJSON(&item)
	err := models.CreateItem(&item)
	if err != nil {
		fmt.Println(err.Error())
		c.JSON(http.StatusNotFound, gin.H{
			"success": false,
			"message": "failed",
			"code":    http.StatusNotFound,
		})
	} else {
		c.JSON(200, gin.H{
			"success": true,
			"message": "success",
			"code":    http.StatusOK,
			"data":    item,
		})
	}
}

func GetItem(c *gin.Context) {
	var item []models.Item
	total, err := models.GetItem(&item, c.Copy())
	if err != nil {
		fmt.Println(err.Error())
		c.JSON(http.StatusNotFound, gin.H{
			"success": false,
			"message": "not_found",
			"code":    http.StatusNotFound,
		})
	} else {
		c.JSON(200, gin.H{
			"success": true,
			"message": "success",
			"code":    http.StatusOK,
			"data":    item,
			"total":   total,
		})
	}
}

func GetItemById(c *gin.Context) {
	id := c.Params.ByName("id")
	var item models.Item
	err := models.GetItemById(&item, id)

	if err != nil {
		fmt.Println(err.Error())
		c.JSON(http.StatusNotFound, gin.H{
			"success": false,
			"message": "not_found",
			"code":    http.StatusNotFound,
		})
	} else {
		c.JSON(200, gin.H{
			"success": true,
			"message": "success",
			"code":    http.StatusOK,
			"data":    item,
		})
	}
}

func UpdateItem(c *gin.Context) {
	var update models.UpdateItemInput
	if err := c.BindJSON(&update); err != nil {
		fmt.Println(err.Error())
		c.JSON(http.StatusBadRequest, gin.H{
			"success": false,
			"message": "errors",
			"code":    http.StatusBadRequest,
		})
		return
	}

	var item models.Item
	id := c.Params.ByName("id")
	err := models.GetItemById(&item, id)
	if err != nil {
		fmt.Println(err.Error())
		c.JSON(http.StatusNotFound, gin.H{
			"success": false,
			"message": "not found",
			"code":    http.StatusNotFound,
		})
		return
	}

	err = models.UpdateItem(&item, &update, id)
	if err != nil {
		fmt.Println(err.Error())
		c.JSON(http.StatusNotFound, gin.H{
			"success": false,
			"message": "not found",
			"code":    http.StatusNotFound,
		})
	} else {
		c.JSON(200, gin.H{
			"success": true,
			"message": "success",
			"code":    http.StatusOK,
			"data":    item,
		})
	}
}

func DeleteItem(c *gin.Context) {
	id := c.Params.ByName("id")
	var item models.Item
	err := models.DeleteItem(&item, id)

	if err != nil {
		fmt.Println(err.Error())
		c.JSON(http.StatusNotFound, gin.H{
			"success": false,
			"message": "not_found",
			"code":    http.StatusNotFound,
		})
	} else {
		c.JSON(200, gin.H{
			"success": true,
			"message": "success",
			"code":    http.StatusOK,
		})
	}
}
