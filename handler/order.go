package handler

import (
	"assignment_2/models"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
)

func CreateOrder(c *gin.Context) {
	var order models.Order
	c.BindJSON(&order)
	err := models.CreateOrder(&order)
	if err != nil {
		fmt.Println(err.Error())
		c.JSON(http.StatusNotFound, gin.H{
			"success": false,
			"message": "failed",
			"code":    http.StatusNotFound,
		})
	} else {
		c.JSON(200, gin.H{
			"success": true,
			"message": "success",
			"code":    http.StatusOK,
			"data":    order,
		})
	}
}

func GetOrder(c *gin.Context) {
	var order []models.Order
	total, err := models.GetOrder(&order, c.Copy())
	if err != nil {
		fmt.Println(err.Error())
		c.JSON(http.StatusNotFound, gin.H{
			"success": false,
			"message": "not_found",
			"code":    http.StatusNotFound,
		})
	} else {
		c.JSON(200, gin.H{
			"success": true,
			"message": "success",
			"code":    http.StatusOK,
			"data":    order,
			"total":   total,
		})
	}
}

func GetOrderById(c *gin.Context) {
	id := c.Params.ByName("id")
	var order models.Order
	err := models.GetOrderById(&order, id)

	if err != nil {
		fmt.Println(err.Error())
		c.JSON(http.StatusNotFound, gin.H{
			"success": false,
			"message": "not_found",
			"code":    http.StatusNotFound,
		})
		return
	} else {
		c.JSON(200, gin.H{
			"success": true,
			"message": "success",
			"code":    http.StatusOK,
			"data":    order,
		})
	}
}

func UpdateOrder(c *gin.Context) {
	var update models.UpdateOrderInput
	if err := c.BindJSON(&update); err != nil {
		fmt.Println(err.Error())
		c.JSON(http.StatusBadRequest, gin.H{
			"success": false,
			"message": "errors",
			"code":    http.StatusBadRequest,
		})
	}

	var order models.Order
	id := c.Params.ByName("id")
	err := models.GetOrderById(&order, id)
	if err != nil {
		fmt.Println(err.Error())
		c.JSON(http.StatusNotFound, gin.H{
			"success": false,
			"message": "not found",
			"code":    http.StatusNotFound,
		})
		return
	}

	err = models.UpdateOrder(&order, &update, id)
	if err != nil {
		fmt.Println(err.Error())
		c.JSON(http.StatusNotFound, gin.H{
			"success": false,
			"message": "not found",
			"code":    http.StatusNotFound,
		})
		return
	}

	err = models.GetOrderById(&order, id)
	if err != nil {
		fmt.Println(err.Error())
		c.JSON(http.StatusNotFound, gin.H{
			"success": false,
			"message": "not found",
			"code":    http.StatusNotFound,
		})
		return
	} else {
		c.JSON(200, gin.H{
			"success": true,
			"message": "success",
			"code":    http.StatusOK,
			"data":    order,
		})
	}
}

func DeleteOrder(c *gin.Context) {
	id := c.Params.ByName("id")
	var order models.Order
	err := models.DeleteOrder(&order, id)

	if err != nil {
		fmt.Println(err.Error())
		c.JSON(http.StatusNotFound, gin.H{
			"success": false,
			"message": "not_found",
			"code":    http.StatusNotFound,
		})
	} else {
		c.JSON(200, gin.H{
			"success": true,
			"message": "success",
			"code":    http.StatusOK,
		})
	}
}
