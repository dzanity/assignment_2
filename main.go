package main

import (
	"assignment_2/config"
	"assignment_2/handler"
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
)

func main() {
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}

	err = config.InitDB()
	if err != nil {
		log.Fatal("Error Load DB")
	}

	r := SetupRouter()

	s := &http.Server{
		Addr:           ":8080",
		Handler:        r,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}
	err = s.ListenAndServe()
	fmt.Println(err.Error())

}

func SetupRouter() *gin.Engine {
	r := gin.Default()

	r.GET("/ping", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"message": "pong",
		})
	})
	//
	api := r.Group("api")
	{
		public := api.Group("")
		{
			public.POST("/orders", handler.CreateOrder)
			public.GET("/orders", handler.GetOrder)
			public.GET("/orders/:id", handler.GetOrderById)
			public.PUT("/orders/:id", handler.UpdateOrder)
			public.DELETE("/orders/:id", handler.DeleteOrder)

			public.POST("/items", handler.CreateItem)
			public.GET("/items", handler.GetItem)
			public.GET("/items/:id", handler.GetItemById)
			public.PUT("/items/:id", handler.UpdateItem)
			public.DELETE("/items/:id", handler.DeleteItem)
		}
	}

	return r
}
