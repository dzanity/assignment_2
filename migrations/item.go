package migration

import (
	"time"
)

type Item struct {
	ItemID      uint   `gorm:"primaryKey" json:"item_id"`
	ItemCode    string `json:"item_code"`
	Description string `json:"description"`
	Quantity    int64  `json:"quantity"`
	OrderID     uint   `json:"order_id"`
	// Order       []Order   `gorm:"constraint:OnUpdate:CASCADE,OnDelete:CASCADE;"`
	CreatedAt time.Time `gorm:"type:datetime"`
	UpdatedAt time.Time `gorm:"type:datetime"`
	// DeletedAt   gorm.DeletedAt `gorm:"index"`
}
