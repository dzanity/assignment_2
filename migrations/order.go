package migration

import (
	"time"
)

type Order struct {
	OrderID      uint      `gorm:"primaryKey" json:"order_id"`
	CustomerName string    `json:"customer_name"`
	OrderedAt    string    `json:"ordered_at"`
	Items        []Item    `gorm:"constraint:OnUpdate:CASCADE,OnDelete:CASCADE;"`
	CreatedAt    time.Time `gorm:"type:datetime" json:"created_at"`
	UpdatedAt    time.Time `gorm:"type:datetime" json:"updated_at"`
	// DeletedAt    gorm.DeletedAt `gorm:"index" json:"deleted_at"`
}
