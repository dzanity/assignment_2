package models

import (
	"assignment_2/config"
	"time"

	"github.com/gin-gonic/gin"
)

type Item struct {
	ItemID uint `gorm:"primaryKey" json:"item_id"`
	// ItemID      uint   `json:"item_id"`
	ItemCode    string `json:"item_code"`
	Description string `json:"description"`
	Quantity    int64  `json:"quantity"`
	OrderID     uint   `json:"order_id"`
	// CreatedAt time.Time `gorm:"type:datetime" json:"created_at"`
	CreatedAt time.Time `json:"created_at"`
	// UpdatedAt time.Time `gorm:"type:datetime" json:"updated_at"`
	UpdatedAt time.Time `json:"updated_at"`
	// DeletedAt   gorm.DeletedAt `gorm:"index" json:"deleted_at"`
}

type Items struct {
	// ItemID uint `gorm:"primaryKey" json:"item_id"`
	ItemID      uint   `json:"item_id"`
	ItemCode    string `json:"item_code"`
	Description string `json:"description"`
	Quantity    int64  `json:"quantity"`
	OrderID     uint   `json:"order_id"`
	// DeletedAt   gorm.DeletedAt `gorm:"index" json:"deleted_at"`
}

type UpdateItemInput struct {
	// ItemID uint `gorm:"primaryKey" json:"item_id"`
	// ItemID      uint   `json:"item_id"`
	ItemCode    string `json:"item_code"`
	Description string `json:"description"`
	Quantity    int64  `json:"quantity"`
}

func CreateItem(item *Item) (err error) {
	item.CreatedAt = time.Now()
	item.UpdatedAt = time.Now()
	if err = config.GetDB().Create(item).Error; err != nil {
		return err
	}
	return nil
}

func GetItem(item *[]Item, c *gin.Context) (count int64, err error) {
	if err = config.GetDB().
		Model(&Item{}).
		Count(&count).
		Find(item).
		Error; err != nil {
		return count, err
	}

	return count, nil
}

func GetItemById(item *Item, id string) (err error) {
	if err = config.GetDB().
		Where("item_id = ?", id).
		First(item).Error; err != nil {
		return err
	}
	return nil
}

func UpdateItem(item *Item, update *UpdateItemInput, id string) (err error) {
	item.ItemCode = update.ItemCode
	item.Quantity = update.Quantity
	item.Description = update.Description

	if err := config.GetDB().Model(&Item{}).
		Where("item_id", id).
		Updates(item).Error; err != nil {
		return err
	}

	return nil
}

func DeleteItem(item *Item, id string) (err error) {
	if err = config.GetDB().Where("item_id = ?", id).
		Delete(item).Error; err != nil {
		return err
	}
	return nil
}
