package models

import (
	"assignment_2/config"
	"time"

	"github.com/gin-gonic/gin"
)

type Order struct {
	OrderID      uint      `gorm:"primaryKey" json:"order_id"`
	CustomerName string    `json:"customer_name"`
	OrderedAt    string    `json:"ordered_at"`
	Items        []*Item   `json:"items"`
	CreatedAt    time.Time `json:"created_at"`
	UpdatedAt    time.Time `json:"updated_at"`
	// DeletedAt    gorm.DeletedAt `gorm:"index" json:"deleted_at"`
}

type UpdateOrderInput struct {
	CustomerName string `json:"customer_name" binding:"required"`
	OrderedAt    string `json:"ordered_at" binding:"required"`
	// Items        []*Item
	Items []Item `json:"items"`
}

func CreateOrder(order *Order) (err error) {
	order.CreatedAt = time.Now()
	order.UpdatedAt = time.Now()
	if err = config.GetDB().Create(order).Error; err != nil {
		return err
	}
	return nil
}

func GetOrder(order *[]Order, c *gin.Context) (count int64, err error) {
	if err = config.GetDB().Preload("Items").
		Model(&Order{}).
		Count(&count).
		Find(order).
		Error; err != nil {
		return count, err
	}

	return count, nil
}

func GetOrderById(order *Order, id string) (err error) {
	if err = config.GetDB().
		Preload("Items").
		Where("order_id = ?", id).
		First(order).Error; err != nil {
		return err
	}
	return nil
}

func UpdateOrder(order *Order, update *UpdateOrderInput, id string) (err error) {
	order.CustomerName = update.CustomerName
	order.OrderedAt = update.OrderedAt

	if err := config.GetDB().Model(&Order{}).
		Where("order_id", id).
		Updates(order).Error; err != nil {
		return err
	}
	//Check dibawah:
	var newItem Items
	for _, v := range update.Items {
		newItem.ItemCode = v.ItemCode
		newItem.Quantity = v.Quantity
		newItem.Description = v.Description
		if err := config.GetDB().Model(&Item{}).
			Where("item_id", v.ItemID).
			Updates(newItem).Error; err != nil {
			return err
		}
	}
	//Kode diatas selalu diupdate ketika datanya tidak ditemukan
	//Tetapi hanya mengupdate data item yang ditemukan
	//Pengen di stop ketika data itemnya tidak ditemukan

	return nil
}

func DeleteOrder(order *Order, id string) (err error) {
	if err = config.GetDB().
		Where("order_id = ?", id).
		Delete(order).Error; err != nil {
		return err
	}
	return nil
}
