# Assignment-2

## Link gitlab: https://gitlab.com/dzanity/assignment_2

- Postman API Docs name: Assignment-2.postman_collection.json (Located in root)
- Check config/database.go (Theres migration's setting inside InitDB())
- Build .env based on your environment
- Run go run main.go

Dependencies:
- GORM
- GODOTENV
- GIN GONIC
- MYSQL